<?php

namespace fehrlich\ImmoScoutAPI\exceptions;

use Exception;

class InvalidTokenException extends Exception
{
}
