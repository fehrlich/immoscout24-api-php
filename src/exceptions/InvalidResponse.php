<?php

namespace fehrlich\ImmoScoutAPI\exceptions;

use Exception;

class InvalidResponse extends Exception
{
    private $response;
    private $msgs;

    public function __construct($msg, $code = 100, $prev = null, $res = null, $msgs = null)
    {
        parent::__construct($msg, $code, $prev);
        $this->msgs = $msgs;
        $this->response = $res;
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function getMessages()
    {
        return $this->msgs;
    }
}
